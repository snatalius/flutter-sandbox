import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

/**
 * Saved Suggestions Screen
 */
class SavedSuggestionsScreen extends StatelessWidget {
  var saved = <WordPair>{};
  var font = const TextStyle(fontSize: 18);

  SavedSuggestionsScreen({ this.saved });

  @override
  Widget build(BuildContext context) {
    final tiles = saved.map( (WordPair pair) {
      return ListTile(
        title: Text(
          pair.asPascalCase,
          style: font,
        ),
      );
    });
    final divided = ListTile.divideTiles(
        context: context,
        tiles: tiles
    ).toList();

    return Scaffold(
      appBar: AppBar(
        title: Text("Saved Suggestions"),
      ),
      body: ListView(children: divided),
    );
  }
}